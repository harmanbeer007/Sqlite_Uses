package agency.brandncodes.sqlite_uses;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    Button submit,read,update,delete;
    EditText u_name,u_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        submit=findViewById(R.id.btnSubmit);
        read=findViewById(R.id.btnRead);
        update=findViewById(R.id.btnUpdate);
        delete=findViewById(R.id.btnDelete);

        u_name=findViewById(R.id.editTextName);
        u_pass=findViewById(R.id.editTextPass);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Database dataOnButton=new Database(MainActivity.this);

                //Supplying data to next page  i.e value of edit text USERNAME,USERPASS
                dataOnButton.addData(u_name.getText().toString().trim(),u_pass.getText().toString().trim());

            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Database dt=new Database(MainActivity.this);
                int result=dt.updateData(u_name.getText().toString().trim(),u_pass.getText().toString().trim());
                if (result!=0)
                {
                    Toast.makeText(MainActivity.this,"UPDATED", Toast.LENGTH_SHORT).show();
                }

//                String oldname=u_name.getText().toString().trim();
//                Intent intent = new Intent(getBaseContext(), updateData.class);
//                intent.putExtra("CHANGE_REQUEST", oldname);
//                startActivity(intent);

            }
        });


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database dt=new Database(MainActivity.this);
                int result=dt.deleteData(u_name.getText().toString().trim());
                if (result!=0)
                {

                }

            }
        });

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Database dataReadButton=new Database(MainActivity.this);
                //String data=dataReadButton.fetchingData();
                HashMap<String,String> clickHashMapData=new HashMap<String,String>();

                clickHashMapData=  dataReadButton.fetchingData(u_name.getText().toString().trim());
                String passw=clickHashMapData.get("user_pass");
                String user=clickHashMapData.get("user_id");

                //  Toast.makeText(MainActivity.this, user+" "+ passw, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getBaseContext(), Nextpage.class);
                intent.putExtra("dataUser", user);
                intent.putExtra("dataPass", passw);
                startActivity(intent);
            }
        });



    }


}
