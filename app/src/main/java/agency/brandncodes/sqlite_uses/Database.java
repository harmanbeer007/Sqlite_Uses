package agency.brandncodes.sqlite_uses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

public class Database extends SQLiteOpenHelper {

    private static final String DB_NAME="USER_DB";

    private static final String TB_NAME="USER_INFO";
    private static final String COL1="U_NAME";
    private static final String COL2="U_PASS";
    private static final int VERSION=1;


    public Database(Context context) {

        super(context, DB_NAME, null,VERSION);

    }

    @Override
    //by using db OBJECT IN BELOW LINE WE WILL INTRACT WITH INTERNAL DB OF ANDROID
    public void onCreate(SQLiteDatabase db) {

        //"CREATE TABLE  USER_INFO(
        // U_NAME VARCHAR (20) NOT NULL
        // U_PASS VARCHAR(20) NOT NULL
        // PRIMARY KEY(U_NAME)
        // );"
        db.execSQL("CREATE TABLE "+TB_NAME+"("+COL1+" VARCHAR(20) NOT NULL ,"+COL2+" VARCHAR(20) NOT NULL ,PRIMARY KEY("+COL1+"));" );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TB_NAME);
        onCreate(db);
    }

    public  void addData(String name,String pass) {


        SQLiteDatabase db=this.getWritableDatabase();

//Contentvalue act as mediator between     and   is like HASHMAP  THIS STORE DATA IN KEY VALUE FORMAT


        ContentValues cnt=new ContentValues();

        cnt.put(COL1,name);
        cnt.put(COL2,pass);

        db.insert(TB_NAME,null,cnt);

        db.close();
    }

    //    public String fetchingData( )
//    {
//        //we RE GOING TO FETCH DATA SO WE NEED A DATA STRUCTURE TO STORE DATA AS IT IS RETURNING VALUE IT IS OF STRING TYPE  ,,,,,
//
//        SQLiteDatabase db=this.getReadableDatabase();
//        //here we put null on the space of selector like "where" statment i.e select * from table "where"
//        Cursor cursor=db.rawQuery("SELECT * FROM   "+TB_NAME,null);
//
//        String data="",data_password;
//        cursor.moveToFirst();
//        do {
//            if (cursor.getColumnCount()!=0)
//            {
//                data=cursor.getString(cursor.getColumnIndex(COL1))+" " +cursor.getString(cursor.getColumnIndex(COL2));
//
//            }
//
//        }
//        while (cursor.moveToNext());
//        db.close();
//        return (data);
//
//    }
    public HashMap<String,String> fetchingData( String name) {
        //we RE GOING TO FETCH DATA SO WE NEED A DATA STRUCTURE TO STORE DATA AS IT IS RETURNING VALUE IT IS OF STRING TYPE  ,,,,,

        SQLiteDatabase db=this.getReadableDatabase();

        HashMap<String,String> readingDataInHashMap=new HashMap<>();

        //here we put null on the space of selector like "where" statment i.e select * from table "where"
        Cursor cursor=db.query(TB_NAME,
                new String[]{COL1,COL2},
                COL1+"=?",
                new String[]{name},
                null,
                null,
                null);

        //cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            readingDataInHashMap.put("user_id",cursor.getString(cursor.getColumnIndex(COL1)));
            readingDataInHashMap.put("user_pass",cursor.getString(cursor.getColumnIndex(COL2)));

        }

        db.close();
        return  readingDataInHashMap;



    }

    public int updateData(String name,String pass) {

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cnt=new ContentValues();


        cnt.put(COL2,pass);



        int result=db.update(TB_NAME,cnt,COL1+"=?",new String[]{name});

        // db.rawQuery("UPDATE "+TB_NAME +" SET "+ COL1+"="+ uname+","+COL2+"="+password +" WHERE " +COL1+"=" +oldUserName,null);
        db.close();

        return result;
    }


    public int deleteData(String name) {

        SQLiteDatabase db=this.getWritableDatabase();

        int result=db.delete(TB_NAME,COL1+"=?",new String[]{name});

        // db.rawQuery("UPDATE "+TB_NAME +" SET "+ COL1+"="+ uname+","+COL2+"="+password +" WHERE " +COL1+"=" +oldUserName,null);
        db.close();

        return result;
    }

}