package agency.brandncodes.sqlite_uses;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Nextpage extends AppCompatActivity {

    TextView tvUser,tvPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nextpage);


        tvUser=findViewById(R.id.txtName);
        tvPass=findViewById(R.id.txtPass);

        String nextScreenUser = getIntent().getStringExtra("dataUser");
        String nextScreenPass = getIntent().getStringExtra("dataPass");
        tvUser.setText(nextScreenUser);
        tvPass.setText(nextScreenPass);
    }
}
